#include <stdio.h>
int get_start()
{
    int input;
    do 
    {
        printf("Enter the starting number: ");
        scanf("%d", &input);
        if( input <= 0)
        {
            printf("The number should be a positive integer: \n");
        }
    }
    while (input < 0);    
    return input;
}
int next_collatz(int i)
{
    int next_number;
    if (i % 2 == 0)
    {
        next_number = i /2;
    }
    else 
    {
        next_number = 3*i + 1;
    }
    return next_number;
}
int main()
{
    int count;
    int start = get_start();
    printf("Collatz sequence: ");
    while( start != 1 )
    {
        printf(" %d,", start);
        int n = next_collatz(start);
        count++;
        start = n;
    }
    printf(" 1");    
    printf("\nLength: %d\n", count+1 ); 
}
